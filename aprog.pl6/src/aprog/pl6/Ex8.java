/*
 * PL6 EX 8 COMPLETO
 */
package aprog.pl6;

import java.util.Scanner;

public class Ex8 {
    
    public static String decToHex(int numDec){
        int resto;
        String numero = "";
        String restoString;
        
        while(numDec!=0){
            resto = numDec % 16;
            numDec=numDec/16;
            switch(resto){
                case 10: restoString = "A";
                    break;
                case 11: restoString = "B";
                    break;
                case 12: restoString = "C";
                    break;
                case 13: restoString = "D";
                    break;
                case 14: restoString = "E";
                    break;
                case 15: restoString = "F";
                    break;
                default: restoString = Integer.toString(resto);
            }
            numero = restoString + numero;
        }
        
        return numero;
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numeroDecimal;
        do{
            System.out.print("Introduza um número para converter para hexadecimal: ");
            numeroDecimal = in.nextInt();
            
            if(numeroDecimal == 0) break;
            
            System.out.println(numeroDecimal + " em hexadecimal é " + decToHex(numeroDecimal));
        }while(numeroDecimal!=0);
    }
    
}
