/*
 * PL6 EX 2
 */
package aprog.pl6;

import java.util.Scanner;

public class Ex2 {
    
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        
        System.out.print("Nome da disciplina: ");
        String disciplina = in.next();
        
        System.out.print("Número de alunos: ");
        int alunos = in.nextInt();
        
        System.out.print("Número de positivas: ");
        int positivas = in.nextInt();
        
        grafico(disciplina, alunos, positivas);
    }
    
    public static void grafico(String disciplina, int alunos, int positivas){
        System.out.println(".................Gráfico.................");
        System.out.println("Disciplina: " + disciplina);
        System.out.print("- Positivas: ");
        for(int i=0; i<positivas; i++){
            System.out.print("*");
        }
        System.out.print("\n");
        System.out.print("- Negativas: ");
        for(int i=0; i<alunos - positivas; i++){
            System.out.print("*");
        }
        System.out.print("\n");
    }
    
}
