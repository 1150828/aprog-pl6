/*
 * PL6 EX 7
 */
package aprog.pl6;

import java.util.Scanner;

public class Ex7 {
    
    public static boolean numOctal(int num){
        while (num > 0) {
            if (num % 10 == 8 || num % 10 == 9) {
                return false;
            }
            num = num / 10;
        }
        
        return true;
    }
    
    public static int octalToDec(int numOctal){
        int resto, ordem=0, numero=0;
        
        while(numOctal!=0){
            resto = numOctal % 10;
            numOctal=numOctal/10;
            numero = (int) (numero + resto * Math.pow(8, ordem));
            ordem++;
        }
        
        return numero;
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num;
        
        do{
            System.out.print("Introduza um número em octal: ");
            num = in.nextInt();
            if(!numOctal(num)) break;
            System.out.println("O número introduzido é " + octalToDec(num) + " em decimal");
        } while(numOctal(num));
        
        System.out.println("O número introduzido não está em base 8");
    }
    
}
