/*
 * PL6 EX 5 COMPLETO
 */
package aprog.pl6;

import java.util.Scanner;

public class Ex5 {
    
    public static int digComuns(int num1, int num2){
        int comuns = 0;
        while(num1!=0 || num2!=0){
            if(num1%10 == num2%10){
                comuns++;
            }
            num1=num1/10;
            num2=num2/10;
        }
        return comuns;
    }
    
    public static void main(String[] args){
        Scanner ler = new Scanner(System.in);
        int N, maxComuns1 = 0, maxComuns2 = 0, maxDigsComuns = 0;
        
        do{
            System.out.print("Quantos pares deseja ler: ");
            N = ler.nextInt();
        }while(N<=0);
        
        for(int i=1; i<N+1; i++){
            System.out.print("Introduza o primeiro número do " + i + "º par: ");
            int numero1 = ler.nextInt();

            System.out.print("Introduza o segundo número do " + i + "º par: ");
            int numero2 = ler.nextInt();

            System.out.println("Este par de números contém " + digComuns(numero1, numero2) + " digitos comuns.");
            
            if(maxDigsComuns<digComuns(numero1, numero2)){
                maxComuns1 = numero1;
                maxComuns2 = numero2;
                maxDigsComuns = digComuns(numero1, numero2);
            }
        }
        
        System.out.println("O par com mais digitos comuns é " + maxComuns1 + " e " + maxComuns2 + " que contém " + maxDigsComuns + " digitos comuns.");
        
    }
    
}
