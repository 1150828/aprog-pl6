/*
 * PL6 EX 7
 */
package aprog.pl6;

import java.util.Scanner;

public class Ex6 {
    
    public static float calcVolume(String tipo, float raio, float altura){
        float volume = 0;
        if(tipo.equalsIgnoreCase("cilindro")){
            volume = (float) (Math.PI * Math.pow(raio, 2) * altura);
        } else if(tipo.equalsIgnoreCase("cone")){
            volume = (float) (Math.PI * Math.pow(raio, 2) * altura * 1/3);
        } else if(tipo.equalsIgnoreCase("esfera")){
            volume = (float) (4/3 * Math.PI * Math.pow(raio, 3));
        } else {
            //System.out.println("Introduza um tipo válido.");
        }
        return volume;
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String tipo;
        float raio=0, altura=1;
        do{
            System.out.print("Escreva o tipo do sólido que deseja calcular o volume: ");
            tipo=in.next();
            
            if(tipo.equalsIgnoreCase("cone") || tipo.equalsIgnoreCase("cilindro")){
                System.out.print("Raio da base do sólido: ");
                raio=in.nextFloat();
                
                System.out.print("Altura do sólido: ");
                altura=in.nextFloat();
            } else if(tipo.equalsIgnoreCase("esfera")){
                System.out.print("Raio da esfera: ");
                raio=in.nextFloat();
            } else if(tipo.equalsIgnoreCase("FIM")){
                break;
            } else {
                System.out.println("Introduza um sólido válido");
                continue;
            }
            System.out.println("O volume do sólido é " + calcVolume(tipo, raio, altura));
        }while(!tipo.equalsIgnoreCase("fim"));
    }
    
}
