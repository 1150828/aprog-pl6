/*
 * PL6 EX 1
 */
package aprog.pl6;

import java.util.Scanner;

public class Ex1 {
    
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int cont = 0;
        String palavra;
        
        do{
            System.out.print("Introduza a palavra a ser verificada se é palíndromo: ");
            palavra=in.next();
            cont++;
        }while(!metodo1(palavra));
        
        if(cont==1){
            System.out.println("Foi introduzida " + cont + " palavra até ser introduzido um palíndromo");
        } else {
            System.out.println("Foram introduzidas " + cont + " palavras até ser introduzido um palíndromo");
        }
    }
    
    public static boolean metodo1 (String pal) {
        boolean resposta = true;
        pal = pal.toLowerCase();
        int tamanho = pal.length();
        for (int i=0 ; i< tamanho /2 ; i++) {
            if (pal.charAt(i) != pal.charAt(tamanho - 1 - i)) {
                resposta = false; break;
            }
        }
        return resposta;
    }
    
    public static boolean metodo2 (String pal) {
        int i, j;
        pal = pal.toLowerCase();
        i = 0; j = pal.length()-1;
        while (i<j && pal.charAt(i) == pal.charAt(j)) {
            i++;
            j--;
        }
        return i>=j;
    }
}
