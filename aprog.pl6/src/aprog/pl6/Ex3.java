package aprog.pl6;

import java.util.Scanner;

public class Ex3 {


    public static float anguloInt(float val1, float val2, float val3) {

        float angulo = (float) Math.acos((Math.pow(val1, 2) + Math.pow(val2, 2) - Math.pow(val3, 2)) / (2 * val1 * val2));

        return (float) Math.toDegrees(angulo);
    }

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        float a, b, c;

        System.out.print("Insira o lado A do triângulo: ");
        a = ler.nextFloat();
        System.out.print("Insira o lado B do triângulo: ");
        b = ler.nextFloat();
        System.out.print("Insira o lado C do triângulo: ");
        c = ler.nextFloat();

        if (anguloInt(a, b, c) + anguloInt(a, c, b) + anguloInt(b, c, a) == 180) {

            System.out.println("Ângulo A^B: " + anguloInt(a, b, c));
            System.out.println("Ângulo A^C: " + anguloInt(a, c, b));
            System.out.println("Ângulo B^C: " + anguloInt(b, c, a));
        } else {
            System.out.println("Os lados fornecidos não formam um triângulo.");
        }
    }
}
