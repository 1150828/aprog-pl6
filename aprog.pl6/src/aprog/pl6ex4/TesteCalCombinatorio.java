package aprog.pl6ex4;

import java.util.Scanner;
/**
* Cálculos vários utilizando métodos da classe CalCombinatorio
*/
    public class TesteCalCombinatorio {
    public static void main(String[] args){

        long n1,n2,res;
        Scanner in = new Scanner(System.in);
        System.out.print("Escreva n: ");
        n1= in.nextLong();
        System.out.print("Escreva p: ");
        n2= in.nextLong();
        res=CalCombinatorio.combinacoes(n1,n2);
        if(res !=-1){
            System.out.println("O resultado é " + res);
        }else{
            System.out.println("Erro!");
        }
    }
    }
