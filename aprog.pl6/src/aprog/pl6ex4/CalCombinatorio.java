package aprog.pl6ex4;

public class CalCombinatorio {
    /**
    * Calcular o factorial de um número
    *
    * @param num número inteiro não negativo
    * @return o valor do factorial do num
    */
    private static long factorial(long num){
        long fact=1;
        for (long i=num; i>1;i=i-1){
            fact=fact*i;
        }
        return fact;
    }
    /**
    * Calcula o combinações de n p a p
    *
    * @param n número inteiro não negativo
    * @param p número inteiro não negativo
    * @return o valor combinações de n p a p ou -1 caso n<p
    */
    public static long combinacoes(long n, long p){
        if(n<p)
        return -1;
        return factorial(n)/(factorial(p)*factorial(n-p));
    }
}